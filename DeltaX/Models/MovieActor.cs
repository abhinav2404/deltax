﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeltaX.Models
{
    public class MovieActor
    {
        public int ID { get; set; }
        public int Movie { get; set; }
        public int Actor { get; set; }
    }
}