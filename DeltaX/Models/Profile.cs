﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DeltaX.Models
{
    public class Profile
    {
        public int PID { get; set; }
        public string PName { get; set; }
        public string Sex { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Bio { get; set; }
        public string Role { get; set; }
    }
}