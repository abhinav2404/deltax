﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace DeltaX.Models
{
    public class Movie
    {
        public int MID { get; set; }
        [DisplayName("Title")]
        public string MName { get; set; }
        [DisplayName("Year of Release")]
        public Nullable<int> ReleaseYear { get; set; }
        public string Plot { get; set; }
        [DisplayName("Poster")]
        public string PosterURL { get; set; }
        public int Producer { get; set; }
    }
}