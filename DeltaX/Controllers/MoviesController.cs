﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using DeltaX.Models;
using AutoMapper;

namespace DeltaX.Controllers
{
    public class MoviesController : Controller
    {
        // GET: Movies
        public ActionResult Index()
        {
            var dal = new Repository();
            var movieList = dal.GetMoviesList();
            var movies = new List<Movie>();
            if (movieList.Any())
            {
                foreach (var movie in movieList)
                {
                    movies.Add(Mapper.Map<Movie>(movie));
                }
            }

            ViewBag.Movies = movies;

            return View();
        }

        // GET: Movies/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            var dal = new Repository();
            var producerList = dal.GetProfiles("p");
            var producers = new List<Models.Profile>();
            if (producerList.Any())
            {
                foreach(var producer in producerList)
                {
                    producers.Add(Mapper.Map<Models.Profile>(Profile));
                }
            }
            ViewBag.Producers = producers;
            return View();
        }

        // POST: Movies/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                Movie movie = new Movie();
                //movie.MName = 

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Movies/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Movies/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
