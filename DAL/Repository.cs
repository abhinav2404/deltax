﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Repository
    {
        private DeltaXDBContext Context;

        public Repository()
        {
            Context = new DeltaXDBContext();
        }

        public List<Movy> GetMoviesList()
        {
            List<Movy> movieList = new List<Movy>();

            try
            {
                movieList = Context.Movies.ToList<Movy>();
            }
            catch
            {
                return movieList;
            }

            return movieList;
        }

        public Movy GetMovieDetails(int movieId)
        {
            Movy movie = null;

            try
            {
                movie = Context.Movies.Where(x => x.MID == movieId).FirstOrDefault();
            }
            catch
            {
                movie = null;
            }

            return movie;
        }

        public bool UpdateMovieDetails(Movy movie)
        {
            bool retValue = false;

            try
            {
                var currentMovie = Context.Movies.Where(x => x.MID == movie.MID).FirstOrDefault();
                if (currentMovie == null) return false;

                currentMovie.MName = movie.MName;
                currentMovie.Plot = movie.Plot;
                currentMovie.PosterURL = movie.PosterURL;
                currentMovie.Producer = movie.Producer;
                currentMovie.ReleaseYear = movie.ReleaseYear;
                Context.SaveChanges();
                return true;
            }
            catch
            {
                retValue = false;
            }

            return retValue;
        }

        public List<Profile> GetProfiles(string role)
        {
            List<Profile> profiles = new List<Profile>();

            try
            {
                profiles = Context.Profiles.Where(x => x.role == role).ToList<Profile>();
            }
            catch
            {
                return profiles;
            }

            return profiles;
        }
    }
}
