﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class DeltaXDBContext : DbContext
    {
        public DeltaXDBContext()
            : base("name=DeltaXDBContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<MovieActor> MovieActors { get; set; }
        public virtual DbSet<Movy> Movies { get; set; }
        public virtual DbSet<Profile> Profiles { get; set; }
    }
}
